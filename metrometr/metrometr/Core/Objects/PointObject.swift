//
//  PointObject.swift
//  metrometr
//
//  Created by Serhii on 5/10/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import Foundation

class Point : NSObject {
    
    var latitude : Double!
    var longitude : Double!
    var title : String!

    init(latitude: Double = 0, longitude: Double = 0, title: String = "") {
        self.latitude = latitude
        self.longitude = longitude
        self.title = title
    }
}
