//
//  AppDelegate.swift
//  metrometr
//
//  Created by Serhii on 5/10/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyBuHUFJmAfeR1EeGQE2OKWmVEAseKRIO6s")
        return true
    }
}

