//
//  ViewController.swift
//  metrometr
//
//  Created by Serhii on 5/10/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

//  MARK: - DataSource
struct Station {
    static let pl = Point(latitude: 50.421021, longitude: 30.520986, title: "Palats")
    static let ol = Point(latitude: 50.432168, longitude: 30.516340, title: "Olimpijska")
    
    static let points = [pl, ol]
}

//MARK: - 

class ViewController: UIViewController {
//    MARK: - Outlets
    @IBOutlet fileprivate weak var mapView: UIView!
    @IBOutlet fileprivate weak var fromView: UIView!
    @IBOutlet fileprivate weak var fromLabel: UILabel!
    @IBOutlet fileprivate weak var distanseView: UIView!
    @IBOutlet fileprivate weak var disnanseLabel: UILabel!
    @IBOutlet fileprivate weak var disnanseActivityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate weak var toView: UIView!
    @IBOutlet fileprivate weak var toLabel: UILabel!

//    MARK: - Variables
    fileprivate var googleMapView: GMSMapView!
    fileprivate var isCalculated = false

//    MARK: - VC Life
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.settings()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    //MARK: - 
    
    /// VC basic preparation
    fileprivate func settings() {
        self.loadMapView()
        self.createMarkers()
        self.configureDistanceLabel()
    }
    
    //MARK: - Maps
    
    /// Load google map to view
    fileprivate func loadMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: 50.425642, longitude: 30.515439, zoom: 13.0)
        googleMapView = GMSMapView.map(withFrame: self.mapView.bounds, camera: camera)
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "gmapstyle", withExtension: "json") {
                googleMapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        self.mapView.addSubview(googleMapView)
    }
    
    /// Create Metro markers on the map
    ///
    /// - Parameter position: position for marker
    fileprivate func createMarkers() {
        for point in Station.points {
            
            guard let wayPointImage = UIImage(named: "metroStationSmall") else {
                debugPrint("metroStation image creation failed")
                return
            }
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude)
            marker.title = point.title
            marker.map = googleMapView
            marker.icon = wayPointImage//newImage

        }
    }

//    MARK: - 
    
    fileprivate func configureDistanceLabel() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.calculateDistance))
        disnanseLabel.isUserInteractionEnabled = true
        disnanseLabel.addGestureRecognizer(tap)
    }
    
    @objc fileprivate func calculateDistance() {
        let startPoint = CLLocation(latitude: Station.ol.latitude, longitude: Station.ol.longitude)
        let endPoint = CLLocation(latitude: Station.pl.latitude, longitude: Station.pl.longitude)
    
        self.fromLabel.text = Station.ol.title
        self.toLabel.text = Station.pl.title
     
        let distance = endPoint.distance(from: startPoint)
        
        self.disnanseLabel.text = String(Int(distance)) + " m"
    }
}



